package com.example.admin.lab01;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by admin on 9/29/17.
 */

public class TagLayout extends LinearLayout {

    Context context;
    LinearLayout tagLayout = this;
    private String[] listItem = {"Vietnamese", "Thai", "Chinese", "American", "Bristish", "Fast food", "Healthy food", "Japanese", "Iraqi", "Italian", "Chicken", "Steak", "Portugese"};

    private List<TextView> listTextView = new ArrayList<>();

    public TagLayout(Context ctx, @Nullable AttributeSet attrs) {
        super(ctx, attrs);
        context = ctx;
        tagLayout.setOrientation(LinearLayout.VERTICAL);

        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                getViewTreeObserver().removeGlobalOnLayoutListener(this);
                calculate();
            }
        });

    }

    private LinearLayout newLinearLayout() {
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.HORIZONTAL);
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 0, 30);
        layout.setLayoutParams(params);
        return layout;
    }

    private TextView newTextView(String text) {
        TextView textView = new TextView(context);
        textView.setText(text);
        textView.setTextColor(getResources().getColor(R.color.colorPrimary));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        textView.setBackgroundResource(R.drawable.rounded_corner_selector);
        textView.setPadding(20,15,20,15);
        textView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        return textView;
    }

    private View newSpaceView() {
        View view = new View(context);
        view.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1f));
        return view;
    }

    private void calculate() {
        LinearLayout llLayout = newLinearLayout();
        tagLayout.addView(llLayout);
        for(int i = 0; i < listItem.length; i++) {
            final TextView newTextView = newTextView(listItem[i]);
            newTextView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    newTextView.setSelected(!newTextView.isSelected());
                    if(newTextView.isSelected()) {
                        newTextView.setTextColor(getResources().getColor(R.color.white));
                    } else {
                        newTextView.setTextColor(getResources().getColor(R.color.colorPrimary));
                    }
                }
            });
            llLayout.addView(newTextView);
            listTextView.add(newTextView);
            llLayout.measure(0,0);
            if(llLayout.getMeasuredWidth() > tagLayout.getMeasuredWidth()) {
                if(llLayout.getChildCount() != 1) {
                    llLayout.removeView(newTextView);
                    i--;
                }
                for(int j = llLayout.getChildCount() - 1; j > 0; j--) {
                    llLayout.addView(newSpaceView(), j);
                }
                llLayout = newLinearLayout();
                tagLayout.addView(llLayout);
            } else {
                if(i == listItem.length - 1) {
                    for(int j = llLayout.getChildCount() - 1; j > 0; j--) {
                        llLayout.addView(newSpaceView(), j);
                    }
                }
            }
        }
    }

    public void setAllTextViewSelected(boolean isSelected){
        for(int i = 0; i < listTextView.size(); i++) {
            listTextView.get(i).setSelected(isSelected);
            if(isSelected) {
                listTextView.get(i).setTextColor(getResources().getColor(R.color.white));
            } else {
                listTextView.get(i).setTextColor(getResources().getColor(R.color.colorPrimary));
            }
        }
    }

}
