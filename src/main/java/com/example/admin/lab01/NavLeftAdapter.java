package com.example.admin.lab01;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 9/28/17.
 */

public class NavLeftAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public static final int TYPE_HEADER = 0;
    public static final int TYPE_MENU = 1;


    private String[] listTitle;
    private TypedArray listIcon;
    private Context context;

    private int selectedPosition;

    NavLeftAdapter(Context ctx, int selected){
        context = ctx;
        selectedPosition = selected;
        listTitle = context.getResources().getStringArray(R.array.nav_left_title);
        listIcon = context.getResources().obtainTypedArray(R.array.nav_left_icon);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if(viewType == TYPE_HEADER) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_header, parent, false);
            HeaderViewHolder headerViewHolder = new HeaderViewHolder(view, viewType);
            return headerViewHolder;
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_list_item, parent, false);
            MenuViewHolder menuViewHolder = new MenuViewHolder(view, viewType);
            return menuViewHolder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof HeaderViewHolder) {

        } else if(holder instanceof MenuViewHolder) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            holder.itemView.startAnimation(animation);
            MenuViewHolder menuViewHolder = (MenuViewHolder)holder;
            int realPos = position - 1;
            menuViewHolder.menuTitle.setText(listTitle[realPos]);
            menuViewHolder.menuIcon.setImageResource(listIcon.getResourceId(realPos, -1));
            if(selectedPosition == position) {
                menuViewHolder.menuIcon.setColorFilter(context.getResources().getColor(R.color.colorPrimary));
                menuViewHolder.menuTitle.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            } else {
                menuViewHolder.menuIcon.setColorFilter(context.getResources().getColor(R.color.tint));
                menuViewHolder.menuTitle.setTextColor(ContextCompat.getColor(context, R.color.tint));
            }
        }
    }

    @Override
    public int getItemCount() {
        return listTitle.length + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0) {
            return TYPE_HEADER;
        } else {
            return TYPE_MENU;
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        ImageView headerImage;
        public HeaderViewHolder(View itemView, int viewType) {
            super(itemView);
            headerImage = itemView.findViewById(R.id.header_image);
        }
    }

    class MenuViewHolder extends RecyclerView.ViewHolder {
        TextView menuTitle;
        ImageView menuIcon;
        public MenuViewHolder(View itemView, int viewType) {
            super(itemView);
            menuIcon = itemView.findViewById(R.id.menu_icon);
            menuTitle = itemView.findViewById(R.id.menu_title);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(selectedPosition == getPosition())
                        return;
                    selectedPosition = getPosition();
                    ((MainActivity)context).changeMainFragment(getPosition());
                    notifyDataSetChanged();
                }
            });
        }
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int pos) {
        selectedPosition = pos;
    }
}
