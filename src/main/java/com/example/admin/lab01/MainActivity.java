package com.example.admin.lab01;

import android.app.Activity;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.app.SearchManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        mRecyclerView = (RecyclerView) findViewById(R.id.nav_left);
        mAdapter = new NavLeftAdapter(this, 1);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close)
        {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        fragmentManager = getSupportFragmentManager();

        changeMainFragment(1);

        Fragment navRightFragment = new NavRightFragment();
        fragmentManager.beginTransaction().replace(R.id.nav_right, navRightFragment).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(Gravity.LEFT);
                return true;
            case R.id.menu_main_filter:
                mDrawerLayout.openDrawer(Gravity.RIGHT);
                return true;
            case R.id.lang_en:
                changLang("en");
                return true;
            case R.id.lang_vi:
                changLang("vi");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void changeMainFragment(int position) {
        mDrawerLayout.closeDrawer(Gravity.LEFT);
        switch(position) {
            case 1:
                Fragment deliveryFragment = new DeliveryFragment();
                fragmentManager.beginTransaction().replace(R.id.main_content, deliveryFragment).commit();
                getSupportActionBar().setTitle(getResources().getStringArray(R.array.nav_left_title)[0]);
                break;
            case 2:
                Fragment exploreFragment = new ExploreFragment();
                fragmentManager.beginTransaction().replace(R.id.main_content, exploreFragment).commit();
                getSupportActionBar().setTitle(getResources().getStringArray(R.array.nav_left_title)[1]);
                break;
        }
    }

    private void changLang(String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        recreate();
    }

}
