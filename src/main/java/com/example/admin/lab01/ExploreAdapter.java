package com.example.admin.lab01;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

/**
 * Created by admin on 9/29/17.
 */

public class ExploreAdapter extends RecyclerView.Adapter<ExploreAdapter.CardViewHolder> {

    Context context;
    private TypedArray listImage;

    ExploreAdapter(Context ctx){
        context = ctx;
        listImage = context.getResources().obtainTypedArray(R.array.photo_image);
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.explore_list_item, parent, false);
        CardViewHolder cardViewHolder = new CardViewHolder(view);
        return cardViewHolder;
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        //Utils.runSlideUpAnimation(holder.itemView, context);
        Random random = new Random();
        holder.cardImage1.setImageResource(listImage.getResourceId(random.nextInt(listImage.length()), -1));
        holder.cardName1.setText("ABC");
        holder.cardType1.setText("ABCDE");
        holder.cardImage2.setImageResource(listImage.getResourceId(random.nextInt(listImage.length()), -1));
        holder.cardName2.setText("ABC");
        holder.cardType2.setText("ABCDE");
    }

    @Override
    public int getItemCount() {
        return listImage.length() * 3;
    }

    class CardViewHolder extends RecyclerView.ViewHolder{
        ImageView cardImage1;
        TextView cardName1;
        TextView cardType1;
        ImageView cardImage2;
        TextView cardName2;
        TextView cardType2;

        public CardViewHolder(View itemView) {
            super(itemView);
            cardImage1 = itemView.findViewById(R.id.card_image_1);
            cardName1 = itemView.findViewById(R.id.card_name_1);
            cardType1 = itemView.findViewById(R.id.card_type_1);
            cardImage2 = itemView.findViewById(R.id.card_image_2);
            cardName2 = itemView.findViewById(R.id.card_name_2);
            cardType2 = itemView.findViewById(R.id.card_type_2);
        }
    }
}
