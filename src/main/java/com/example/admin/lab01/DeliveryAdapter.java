package com.example.admin.lab01;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by admin on 9/28/17.
 */

public class DeliveryAdapter extends RecyclerView.Adapter<DeliveryAdapter.CardViewHolder>{

    Context context;
    private TypedArray listImage;

    DeliveryAdapter(Context ctx){
        context = ctx;
        listImage = context.getResources().obtainTypedArray(R.array.photo_image);
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.delivery_list_item, parent, false);
        CardViewHolder cardViewHolder = new CardViewHolder(view);
        return cardViewHolder;
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        /*Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
        holder.itemView.startAnimation(animation);*/
        //Utils.runSlideUpAnimation(holder.itemView, context);
        holder.cardImage.setImageResource(listImage.getResourceId(position, -1));
        holder.cardName.setText("ABC");
        holder.cardType.setText("ABCDE");
    }

    @Override
    public int getItemCount() {
        return listImage.length();
    }

    class CardViewHolder extends RecyclerView.ViewHolder{
        ImageView cardImage;
        TextView cardName;
        TextView cardType;

        public CardViewHolder(View itemView) {
            super(itemView);
            cardImage = itemView.findViewById(R.id.card_image);
            cardName = itemView.findViewById(R.id.card_name);
            cardType = itemView.findViewById(R.id.card_type);
        }
    }
}
