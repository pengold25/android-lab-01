package com.example.admin.lab01;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by admin on 9/29/17.
 */

public class NavRightFragment extends Fragment {

    private Button btnAll;
    private Button btnNone;
    private TagLayout tagLayout;

    public NavRightFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_nav_right, container, false);
        btnAll = (Button) rootView.findViewById(R.id.btn_all);
        btnNone = (Button) rootView.findViewById(R.id.btn_none);
        tagLayout = (TagLayout) rootView.findViewById(R.id.tag_layout);

        btnAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tagLayout.setAllTextViewSelected(true);
            }
        });

        btnNone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tagLayout.setAllTextViewSelected(false);
            }
        });
        return rootView;
    }
}
